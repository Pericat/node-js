/*function isAuth (req, res, next) {
    if (req.session.user) {
        return next
    }
    return res.redirect('/login')
}*/


router.get('/login', (req, res) => {
    res.render('login')
})


router.get('/', (req, res) => {
    res.render('index', {username: req.session.user})
})

router.get('/:id',  (req, res) => {
    res.render('user')
})


module.exports = router
