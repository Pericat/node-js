const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt')
const saltRounds = 10

/*function isAuth (req, res, next) {
    if (req.session.user) {
        return next
    }
    return res.redirect('/login')
}*/


router.get('/login', (req, res) => {
    res.render('login')
})


router.get('/', (req, res) => {
    res.render('index', {username: req.session.user})
})

router.get('/user',  (req, res) => {
    res.render('user')
})
router.post('/user', (req, res) => {
    req.session.user = req.body.user
    req.session.pass = req.body.pass
    result = bcrypt.hash(req.session.pass, saltRounds).then(hash => {
        res.render('user', {username: req.session.user, passwordHashed: hash})
    })
      .catch(err => console.error(err.message))
})

module.exports = router
