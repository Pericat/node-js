const express = require('express');
const path = require('path')
const routes = require('./controller/router');
var session = require('express-session')

const app = express();

app.use(express.urlencoded({extended: false}), session({
    secret: 'whatever the hell you need',
    resave: false,
    saveUninitialized: true
  }))

app.use(express.json())

app.set('view engine', 'ejs')

app.set('views', path.join(__dirname, 'views'))

app.use('/', routes,)

app.listen(3000)
