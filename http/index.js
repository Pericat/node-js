const http = require('http')

const options = {
    hostname: 'localhost',
    port: 3000,
    path: '/',
}

const requestListener = function (req, res) {
  res.end(`<html><body><h1>Hello HTTP</h1></body></html>`);
};

const server = http.createServer(requestListener);
server.listen(options, () => {
    console.log(`Server is running`);
});